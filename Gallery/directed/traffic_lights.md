---
copyright: Copyright &#169; 1996-2004 AT&amp;T.  All rights reserved.
redirect_from:
  - /_pages/Gallery/directed/traffic_lights.html
layout: gallery
title: Traffic Lights
svg: traffic_lights.svg
gv_file: traffic_lights.gv.txt
img_src: traffic_lights.png
---
"I played some days with making an interface between our ConceptBase system (essentially a database system to store models) and graphviz. One example graph is attached. It is a so-called petri net for Dutch traffic lights. The example is actually taken from a book by Wil van der Aalst." Contributed by Manfred Jeusfeld.
