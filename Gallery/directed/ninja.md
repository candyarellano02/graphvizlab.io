---
layout: gallery
title: Ninja Build System
svg: ninja.svg
gv_file: ninja.gv.txt
img_src: ninja.png
---
[Ninja](https://ninja-build.org/), a small, fast build system, can output graphviz dependency graphs.

This is Ninja's dependency graph when building Ninja itself, auto-generated by running

```sh
$ ninja -t graph [mytarget]
```

See the [Ninja manual](https://ninja-build.org/manual.html) for details.
