---
defaults:
- '""'
flags:
- map
- svg
minimums: []
name: tailhref
types:
- escString
used_by: E
---
Synonym for [`tailURL`](#d:tailURL).
