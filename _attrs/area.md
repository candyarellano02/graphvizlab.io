---
defaults:
- '1.0'
flags:
- patchwork
minimums:
- '>0'
name: area
types:
- double
used_by: NC
---
Indicates the preferred area for a node or empty cluster when laid out by patchwork.
