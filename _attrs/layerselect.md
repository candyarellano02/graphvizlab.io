---
defaults:
- '""'
flags: []
minimums: []
name: layerselect
types:
- layerRange
used_by: G
---
Selects a list of layers to be emitted.
