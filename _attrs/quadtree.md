---
defaults:
- normal
flags:
- sfdp
minimums: []
name: quadtree
types:
- quadType
- bool
used_by: G
---
Quadtree scheme to use.

* `quadtree=true` aliases `quadtree=normal`
* `quadtree=false` aliases `quadtree=none`
* `quadtree=2` aliases `quadtree=fast`
