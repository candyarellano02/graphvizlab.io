---
defaults: []
flags:
- write
minimums: []
name: tail_lp
types:
- point
used_by: E
---
Position of an edge's tail label, [in points](#points).

The position indicates the center of the label.
