---
defaults:
- '""'
flags:
- cmap
- svg
minimums: []
name: edgetooltip
types:
- escString
used_by: E
---
Tooltip annotation attached to the non-label part of an edge.

Used only if the edge has a [`URL`](#d:URL) or [`edgeURL`](#d:edgeURL) attribute.
