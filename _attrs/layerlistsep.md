---
defaults:
- '","'
flags: []
minimums: []
name: layerlistsep
types:
- string
used_by: G
---
Specifies the separator characters used to split an attribute of type
[`layerRange`](#k:layerRange) into a list of ranges.
