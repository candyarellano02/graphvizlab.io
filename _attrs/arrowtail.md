---
defaults:
- normal
flags: []
minimums: []
name: arrowtail
types:
- arrowType
used_by: E
---
Style of arrowhead on the tail node of an edge.
This will only appear if the [`dir` attribute](#d:dir)
is `back` or `both`.

See the [limitation](#h:undir_note).
